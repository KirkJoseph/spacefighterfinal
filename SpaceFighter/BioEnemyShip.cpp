
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	// inialize defaults for this object
	SetSpeed(150); // set default speed to 150
	SetMaxHitPoints(1); // set default number of hitpoints to 1
	SetCollisionRadius(20); // set default collision radius to 20
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		// if enemy is active, update x coordinate based on the formula below, and y coordinate by enemy speed * time elapsed
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		// if enemy is no longer on the screen based on previous math, deactivate it
		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
