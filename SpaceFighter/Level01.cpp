

#include "Level01.h"
#include "BioEnemyShip.h"
#include "BossShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *pBossTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");


	// number of enemies
	const int COUNT = 21;

	// enemy ship starting x position
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	// enemy ship delay before they appear on screen
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 5.0; // start delay
	Vector2 position;
	//Vector2 bossposition;

	//bossposition.Set(xPositions[0] * Game::GetScreenWidth(), -pBossTexture->GetCenter().Y);

	// for loop to add enemy objects into game based on xposition/delay information in arrays
	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	//delay = delay + 7;
	/*
	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		bossposition.Set(xPositions[i] * Game::GetScreenWidth(), -pBossTexture->GetCenter().Y);

		BossShip *pBoss = new BossShip();
		pBoss->SetTexture(pBossTexture);
		pBoss->SetCurrentLevel(this);
		pBoss->Initialize(bossposition, (float)delay);
		AddGameObject(pBoss);
	}
	*/
	//set up boss ship
	//delay = delay + 5;

	//BossShip *pBoss = new BossShip();
	//pBoss->SetTexture(pBossTexture);
	//pBoss->SetCurrentLevel(this);
	//pBoss->Initialize(bossposition, (float)delay);
	//AddGameObject(pBoss);
	
	Level::LoadContent(pResourceManager);

}

